#!/bin/bash

########### Make Directories ###################################################

mkdir -p model output align

########### Language Model Training ############################################

data="kyoto-tunesmall"

# Train the language model
script/train-trigram.py data/$data.en > model/trigram.txt

## Reordering
#echo "Reordering!"
#script/pre-ord.py < data/kyoto-test-upto20.ja > data/kyoto-test-upto20.ja.ord
#script/pre-ord.py < data/kyoto-dev-upto20.ja > data/kyoto-dev-upto20.ja.ord
#script/pre-ord.py < data/kyoto-dev.ja > data/kyoto-dev.ja.ord
#script/pre-ord.py < data/kyoto-tunesmall.ja > data/kyoto-tunesmall.ja.ord
#script/pre-ord.py < data/kyoto-tunetrain.ja > data/kyoto-tunetrain.ja.ord
#script/pre-ord.py < data/kyoto-tune.ja > data/kyoto-tune.ja.ord



echo "Start here"
########### Alignment ##########################################################

# 1) Calculate the dice coefficient using the small corpus
script/train-dice.py data/$data.ja.ord data/$data.en > model/dice-tunesmall.ja-en

# 2) Create a score file in reverse order
script/reverse-score.py model/dice-tunesmall.ja-en > model/dice-tunesmall.en-ja

# 3) Use the max-score rule to calculate these in ja-en and en-ja order
script/max-score.py data/kyoto-tune.ja.ord data/kyoto-tune.en model/dice-tunesmall.ja-en > align/$data.ja-en
script/max-score.py data/kyoto-tune.en data/kyoto-tune.ja.ord model/dice-tunesmall.en-ja > align/$data.en-ja

# 4) Use the "intersection" heuristic to calculate the actual alignments
script/grow-diag-final.py align/$data.ja-en align/$data.en-ja > align/$data.grow-diag.ja-en

########### Phrase Training ####################################################

# Do phrase extraction
script/phrase-extract.py data/$data.{ja,en} align/$data.grow-diag.ja-en > model/phrase-table.txt

########### Translation ########################################################

# Do translation
script/decoder.py --n_gram 3 --prob_file model/phrase-table.txt --lm_file model/trigram.txt < data/kyoto-dev-upto20.ja.ord | tee output/kyoto-dev-upto20.en

# Do evaluation
script/bleu.py output/kyoto-dev-upto20.en data/kyoto-dev-upto20.en
