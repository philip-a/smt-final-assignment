#!/usr/bin/python

#############################################################################
# Combine two alignments in the source-target and target-source direction.  #
# Use the "intersection" heuristic, using only alignments in both directions#
#  Usage: intersect.py SRCTRG_ALIGNMENTS TRGSRC_ALIGNMENTS                  #
#############################################################################

# Load the system library
import sys

# Read in the reference alignments and system alignments
srctrg_aligns = open(sys.argv[1], "r").readlines()
trgsrc_aligns = open(sys.argv[2], "r").readlines()

# For each line in both files, add the counts
for srctrg_align, trgsrc_align in zip(srctrg_aligns, trgsrc_aligns):
    # Remove the white space from each sentence
    srctrg_align = srctrg_align.strip()
    trgsrc_align = trgsrc_align.strip()

    # For both sentences, add them to a set of unique alignments.
    srctrg_unique = {}
    if len(srctrg_align) > 1:
        for srctrg in srctrg_align.split(" "):
            (src, trg) = map(int,srctrg.split("-"))
            srctrg_unique[src,trg] = 1
    # Note that we have to reverse the order of the source/target here
    trgsrc_unique = {}
    if len(trgsrc_align) > 1:
        for trgsrc in trgsrc_align.split(" "):
            (trg, src) = map(int,trgsrc.split("-"))
            trgsrc_unique[src,trg] = 1
    
    # magic
    src_align = set()
    trg_align = set()
    
    #union and intersection
    output = {}
    union_unique = {}
    for (src,trg) in srctrg_unique:
        union_unique[src,trg] = 1
        if (src, trg) in trgsrc_unique:
            output[src,trg] = 1 # include anything in the intersection
            src_align.add(src)
            trg_align.add(trg)

    for (src,trg) in trgsrc_unique:
        union_unique[src,trg] = 1

    # grow-diag-final
    added = True
    while added:
        added = False
        for (x,y) in union_unique:
            if (x,y) not in output:
                vertical_al = (x+1,y) in output or (x-1,y) in output
                horizontal_al = (x,y+1) in output or (x,y-1) in output
                if (not y in trg_align and not x in src_align) or (vertical_al != horizontal_al):
                    # align them
                    output[x,y]=1
                    trg_align.add(y)
                    src_align.add(x)
                    added = True
             

    # finally print the output
    print " ".join("%d-%d" % (src, trg) for (src, trg) in output.keys())



    