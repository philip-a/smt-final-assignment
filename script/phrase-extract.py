#!/usr/bin/python

#############################################################################
# This is a file that reads source, target, and alignments and outputs      #
#    phrase translation probabilities P(f|e).                               #
#    The algorithm is based on Figure 5.1 in Franz Och's thesis             #
#    http://sylvester.bth.rwth-aachen.de/dissertationen/2003/059/03_059.pdf #
#  Usage: grade-align.py SRC TRG ALIGN                                      #
#############################################################################

# Load the system and itertools library
import sys, itertools
from collections import defaultdict

# Settings
max_len = 5 # The maximum length of a phrase

# Read in the reference alignments and system alignments
src_file = open(sys.argv[1], "r")
trg_file = open(sys.argv[2], "r")
align_file = open(sys.argv[3], "r")

# A function to tell if alignments are quasi-consecutive
def quasi_consecutive(TP, A):
    TPlist = sorted(list(TP))
    for a,b in [ (TPlist[i-1], TPlist[i]) for i in range(1, len(TPlist)) ]:
        for i in range(a+1, b):
            if len(A[i]) > 0:
                return False
    return len(TP) > 0

# Save the counts of (f,e), f, and e
srctrg_cnt = defaultdict(lambda: 0)
src_cnt = defaultdict(lambda: 0)
trg_cnt = defaultdict(lambda: 0)

# Read all of the lines and process them one by one
for (src, trg, align) in itertools.izip(src_file,trg_file,align_file):
    # Get the source and target
    src_word = src.strip().split(" ")
    I = len(src_word)
    trg_word = trg.strip().split(" ")
    J = len(trg_word)
    # Get the alignments
    Ai = map(lambda x: [], range(I))
    Aj = map(lambda x: [], range(J))
    for (i,j) in map(lambda x: x.split("-"), align.strip().split(" ")):
        i = int(i)
        j = int(j)
        Ai[i].append(j)
        Aj[j].append(i)
    # Iterate over all of the phrases
    #  See the phrase-extract algorithm in Och's thesis
    for i1 in range(I):
        for i2 in range(i1, min(I, i1+max_len)):
            TP = set(itertools.chain.from_iterable(Ai[i1:i2+1]))
            # print i1, i2, TP
            if quasi_consecutive(TP, Aj):
                j1 = min(TP)
                j2 = max(TP)
                SP = set(itertools.chain.from_iterable(Aj[j1:j2+1]))
                if len(SP) > 0 and min(SP) >= i1 and max(SP) <= i2:
                    src_phrase = " ".join(src_word[i1:i2+1])
                    ja = j1
                    while ja >= 0:
                        jb = j2
                        while jb-ja < max_len:
                            trg_phrase = " ".join(trg_word[ja:jb+1])
                            srctrg_cnt[src_phrase, trg_phrase] += 1
                            src_cnt[src_phrase] += 1
                            trg_cnt[trg_phrase] += 1
                            jb += 1
                            if jb == J or len(Aj[jb]) > 0: break
                        ja -= 1
                        if ja == -1 or len(Aj[ja]) > 0: break

# Print out "f ||| e ||| P(e|f) ||| P(f|e)"
for ((src, trg), cnt) in sorted(srctrg_cnt.items()):
    print "%s ||| %s ||| %g ||| %g" % (src, trg, float(cnt)/src_cnt[src], float(cnt)/trg_cnt[trg])

# Close the files
src_file.close()
trg_file.close()
align_file.close()
