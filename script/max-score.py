#!/usr/bin/python

#########################################################################
# This is a script to calculate alignments according to the "max-score" #
# criterion, where each word in the source sentence is aligned to the   #
# target word that has the maximum score                                #
#   Usage: max-score.py SRC_FILE TRG_FILE SCORE_FILE > ALIGNMENTS       #
#########################################################################

# Load the system library
import sys

# Read in the scores for each source/target pair from the alignment file
from collections import defaultdict
srctrg_scores = defaultdict(lambda: -1e6)
with open(sys.argv[3], "r") as score_file:
    for line in score_file:
        (src, trg, score) = line.strip().split(" ")
        srctrg_scores[src, trg] = score

# Read in the source and target files
src_sents = open(sys.argv[1], "r").readlines()
trg_sents = open(sys.argv[2], "r").readlines()

# For each line in both files, find the alignments
sent = 0
for src_sent, trg_sent in zip(src_sents, trg_sents):
    # Print a note on every 10,000th sentence
    sent += 1
    if sent % 10000 == 0:
        print >> sys.stderr, sent
    # Remove the white space from each sentence
    src_sent = src_sent.strip()
    trg_sent = trg_sent.strip()
    # Create a list saving our alignments
    alignments = []
    # For each word in the source, find the target with the highest value
    for src_id, src in enumerate(src_sent.split(" ")):
        # Initialize best score to a small value, and best ID to -1 (no align)
        best_score = -1e6
        best_id = -1
        # Look at all the target words and find the one with the best score
        for trg_id, trg in enumerate(trg_sent.split(" ")):
            if srctrg_scores[src, trg] > best_score:
                best_score = srctrg_scores[src, trg]
                best_id = trg_id
        # If we have found an alignment, add it
        if best_id != -1:
            alignments.append( "%d-%d" % (src_id, best_id) )
    # Finally, print the alignments we have found
    print " " + " ".join(alignments)
