#!/usr/bin/python
# This program tests a bigram model
#  Usage: test-bigram.py BIGRAM_FILE TEST_FILE

import sys
import math

# The interpolation coefficients for 
lambda_unk = 0.1
lambda_2 = 0.6
lambda_1 = 1-lambda_unk-lambda_2

# The number of possible unknown words in the vocabulary
V = 10000

# The number of words and total entropy
W = 0
H = 0

# Load in the probabilities from a file
from collections import defaultdict
probs = defaultdict(lambda: 0)
with open(sys.argv[1], "r") as probs_file:
    for line in probs_file:
        line = line.strip()
        (ngram, prob) = line.split("\t")
        probs[ngram] = float(prob)

# Read in every line from the file
input_file = open(sys.argv[2], "r")
for line in input_file:
    # Split it into words and add probabilities
    line = line.strip()
    words = line.split(" ")
    words.insert(0, "<s>")
    words.append("</s>")
    # For every word
    for i in range(1, len(words)):
        W += 1
        # Calculate its probability interpolating
        # the unigram, bigram, and unknown word probabilities
        P = lambda_2 * probs["%s %s" % (words[i-1], words[i])] \
            + lambda_1 * probs[words[i]] \
            + lambda_unk / V
        # And convert into entropy
        H += -math.log(P,2)

print "entropy = %f" % float(H/W)
