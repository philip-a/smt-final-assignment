#!/usr/bin/python

#############################################################################
# Combine two alignments in the source-target and target-source direction.  #
# Use the "intersection" heuristic, using only alignments in both directions#
#  Usage: intersect.py SRCTRG_ALIGNMENTS TRGSRC_ALIGNMENTS                  #
#############################################################################

# Load the system library
import sys

# Read in the reference alignments and system alignments
srctrg_aligns = open(sys.argv[1], "r").readlines()
trgsrc_aligns = open(sys.argv[2], "r").readlines()

# For each line in both files, add the counts
for srctrg_align, trgsrc_align in zip(srctrg_aligns, trgsrc_aligns):
    # Remove the white space from each sentence
    srctrg_align = srctrg_align.strip()
    trgsrc_align = trgsrc_align.strip()
    # For both sentences, add them to a set of unique alignments.
    srctrg_unique = {}
    if len(srctrg_align) > 1:
        for srctrg in srctrg_align.split(" "):
            srctrg_unique[srctrg] = 1
    # Note that we have to reverse the order of the source/target here
    trgsrc_unique = {}
    if len(trgsrc_align) > 1:
        for trgsrc in trgsrc_align.split(" "):
            (trg, src) = trgsrc.split("-")
            trgsrc_unique["%s-%s" % (src, trg)] = 1
    # Create a list for the alignments
    alignments = []
    # Count the co-occurrence statistics for the reference and system
    for srctrg in srctrg_unique.keys():
        if srctrg in trgsrc_unique:
            alignments.append(srctrg)
    print " ".join(alignments)
