#!/usr/bin/python

#############
# This is a script to calculate the dice coefficient for every word pair in
# a parallel corpus

# Load the system library
import sys

# Read in the source and target files
src_sents = open(sys.argv[1], "r").readlines()
trg_sents = open(sys.argv[2], "r").readlines()

# Create three dictionaries, two for source/target occurrence and one for
# co-occurence
from collections import defaultdict
src_counts = defaultdict(lambda: 0)
trg_counts = defaultdict(lambda: 0)
srctrg_counts = defaultdict(lambda: 0)

# For each line in both files, add the counts
sent = 0
for src_sent, trg_sent in zip(src_sents, trg_sents):
    # Print on every 10,000th sentence
    sent += 1
    if sent % 10000 == 0:
        print >> sys.stderr, sent
    # Remove the white space from each sentence
    src_sent = src_sent.strip()
    trg_sent = trg_sent.strip()
    # For both sentences, add them to a set of unique words. This is because we
    # only count once, even if the word occurs 2+ times in the sentence
    src_unique = {}
    for src in src_sent.split(" "):
        src_unique[src] = 1
    trg_unique = {}
    for trg in trg_sent.split(" "):
        trg_unique[trg] = 1
    # Count the occurrence statistics for the source and target
    for src in src_unique.keys():
        src_counts[src] += 1
    for trg in trg_unique.keys():
        trg_counts[trg] += 1
    # Count the co-occurrence statistics for the source and target
    for src in src_unique.keys():
        for trg in trg_unique.keys():
            srctrg_counts[src, trg] += 1

# For every pair of words that co-occurs once, calculate its Dice coefficient
for src, trg in srctrg_counts.keys():
    dice = 2 * float(srctrg_counts[src,trg]) / (src_counts[src] + trg_counts[trg])
    print "%s %s %f" % (src, trg, dice)
