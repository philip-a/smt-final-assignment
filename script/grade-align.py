#!/usr/bin/python

#############################################################################
# This is a file that grades alignments according to precision, recall, and #
# alignment F-measure                                                       #
#  Usage: grade-align.py REFERENCE_ALIGNMENTS SYSTEM_ALIGNMENTS             #
#############################################################################

# Load the system library
import sys

# Read in the reference alignments and system alignments
ref_aligns = open(sys.argv[1], "r").readlines()
sys_aligns = open(sys.argv[2], "r").readlines()

# Count the number of alignments output in the reference, system output, and
# the number of alignments that match both
ref_count = 0
sys_count = 0
refsys_count = 0

# For each line in both files, add the counts
for ref_align, sys_align in zip(ref_aligns, sys_aligns):
    # Remove the white space from each sentence
    ref_align = ref_align.strip()
    sys_align = sys_align.strip()
    # For both sentences, add them to a set of unique alignments.
    ref_unique = {}
    for ref in ref_align.split(" "):
        ref_unique[ref] = 1
    sys_unique = {}
    for sys in sys_align.split(" "):
        sys_unique[sys] = 1
    # Count the occurrence statistics for the reference and system
    ref_count += len(ref_unique)
    sys_count += len(sys_unique)
    # Count the co-occurrence statistics for the reference and system
    for ref in ref_unique.keys():
        if ref in sys_unique:
            refsys_count += 1

# Calculate the statistics and print them out
P = float(refsys_count) / sys_count
R = float(refsys_count) / ref_count
F = 2*P*R/(P+R)
print "Alignment Precision: %f" % P
print "Alignment Recall:    %f" % R
print "Alignment F-Measure: %f" % F
