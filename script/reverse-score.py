#!/usr/bin/python

import sys

# This file reverses the order of alignments for convenience
with open(sys.argv[1], "r") as align_file:
    for line in align_file:
        (src, trg, val) = line.strip().split()
        print trg, src, val
