#!/usr/bin/python

#############################################################################
# This is a very simple phrase-based machine translation decoder            #
#  Usage: decoder.py PHRASE_TABLE BIGRAM < INPUT > OUTPUT                   #
#############################################################################

###### Libraries and sanity check

import sys
import math
import itertools
import argparse
from collections import defaultdict

parser = argparse.ArgumentParser(description="Run decoding")
parser.add_argument('--lm_file', type=str)
parser.add_argument('--prob_file', type=str)
parser.add_argument('--n_best', default=0, type=float)
parser.add_argument('--n_gram', default=0, type=int)
args = parser.parse_args()

###### Settings 

# Weights for the log-linear model in translation
lambda_lm = 1.0       # Weight of the LM probability P(e_i | e_{i-1})
lambda_tm = 1.0       # Weight of the TM probability P(f|e)
lambda_dist = 0.005     # Distortion model weight alpha_o
lambda_phrase = 0.0   # Phrase score
lambda_word = 2.0     # Word score

# Weights for the language model
if args.n_gram == 3:
	lambda_unk = 0.1
	lambda_3 = 0.3
	lambda_2 = 0.4
	lambda_1 = 1-lambda_3-lambda_unk-lambda_2
else:
    lambda_2 = 0.6
    lambda_unk = 0.1
    lambda_1 = 1.0 - lambda_2 - lambda_unk

V = 100000

# Search setting for the decoder
dist_lim = 10 # Distortion limit
beam = 200   # Beam width

# For each source phrase, only use the top N translations (for efficiency)
max_trg_phrase = 30

generate_n_best_list = 0
###### Read in the language model

# Load the language model
lm = defaultdict(lambda: 0)
with open(args.lm_file, "r") as lm_file:
    for line in lm_file:
        line = line.strip()
        (ngram, prob) = line.split("\t")
        lm[ngram] = float(prob)

# A function to calculate the log probability of a word sequence.
# Note that because we are using a bigram language model, the first word is
# context, and we do not calculate its probability.
gg = 0
def calc_lm_log_prob(words):
    # For every word
    prob = 0.0
   
    if args.n_gram == 2:
	    for i in range(1, len(words)):
	        # Calculate its probability interpolating unigram/bigram/unk
	        P = lambda_2 * lm["%s %s" % (words[i-1], words[i])] \
	            + lambda_1 * lm[words[i]] \
	            + lambda_unk / V
	        # And convert into log probability
	        prob += math.log(P)
    else:
        for i in range(2, len(words)):
	        # Calculate its probability interpolating unigram/bigram/unk
	        P = lambda_3 * lm["%s %s %s" % (words[i-2],words[i-1], words[i])] \
	            + lambda_2 * lm["%s %s"%(words[i-1],words[i])] \
                + lambda_1 * lm[words[i]] \
	            + lambda_unk / V
	        # And convert into log probability
	        prob += math.log(P)
    return prob
    
###### Read in the phrase table

# Load the phrase table
pt = defaultdict(lambda: [])
max_src_len = 0
with open(args.prob_file, "r") as pt_file:
    for line in pt_file:
        (src, trg, p_t_given_s, p_s_given_t) = line.strip().split(" ||| ")
        src_words = trg.split(" ")
        max_src_len = max(len(src_words), max_src_len)
        trg_words = trg.split(" ")
        pt[src].append( (trg_words, math.log(float(p_t_given_s)), math.log(float(p_s_given_t))) )

# For every source phrase in the translation model, sort by the probability
#  of the target phrase, and remove all but the top N
for trg_list in pt.values():
    if len(trg_list) > max_trg_phrase:
        trg_list.sort(key=lambda x: -x[1])
        trg_list = trg_list[:max_trg_phrase]

###### Perform translation one line at a time

for line in sys.stdin:
    # Split into words and get the length
    src = line.strip().split(" ")
    J = len(src)
    # Find all of the phrases that match the source
    matched = defaultdict(lambda: [])
    for j1 in range(0, J):
        for j2 in range(j1+1, min(J+1, j1+1+max_src_len)):
            srcid = " ".join(src[j1:j2])
            if srcid in pt:
                matched[j1,j2] = pt[srcid]
            # Add unknown words to the translation model
            elif j2 == j1+1:
                matched[j1,j2] = [ ([src[j1]], 0.0, 0.0) ]
    # Initialize the stacks.
    # Each member of the stack is a tuple of:
    #  1) source coverage vector
    #  2) last span translated
    #  3) last target word
    # Each associated value is
    #  1) Best score of the path leading to this member
    #  2) Best previous state
    #  3) Target words leading to this previous state
    stacks = [dict() for i in range(J+1)]
    stacks[0][ ("0"*J, (-1,0), "<s>","<s>") ] = (0.0, None, None,None)

    # Starting here is the stack decoding algorithm (Viterbi search)
    # Iterate over each stack of hypotheses with l_finished words translated
    for l_finished, stack in enumerate(stacks[:-1]):
        # Get the stack in order of descending score
        ordered_stack = stack.items()
        ordered_stack.sort(key=lambda x: -x[1][0])
        for l_stackid in ordered_stack[:beam]:
            ((l_cover, l_span, l_trg1, l_trg2), (l_score, l_prev_fin, l_prev, l_trg_words)) = l_stackid
            l_firstuncovered = l_cover.index("0")
            # For all phrases starting within the distortion limit
            for j1 in range( max(0, l_span[1]-dist_lim), min(l_span[1]+dist_lim,J)):
                n_cover = str(l_cover)
                n_finished = l_finished
                # For various lengths of phrases
                for j2 in range(j1+1, min(J+1, j1+1+max_src_len)):
                    # If j2 is already covered, don't translate it again
                    if l_cover[j2-1] == "1":
                        break 
                    # If the first uncovered word will no longer be translatable due to the
                    # distortion limit, don't translate this phrase
                    if j1 != l_firstuncovered and j2-l_firstuncovered > dist_lim:
                        break
                    # Update the coverage/translated span
                    n_cover = n_cover[0:j2-1]+"1"+n_cover[j2:]
                    n_span = (j1, j2)
                    n_finished += 1
                    # For every possible set of target words
                    for (n_trg_words, log_t_given_s, log_s_given_t) in matched[j1, j2]:
                        # Get the last target word and stack id
                        lm_words = [l_trg1,l_trg2]+n_trg_words
                        n_trg1 = lm_words[-2]
                        n_trg2 = lm_words[-1]
                        n_stackid = (n_cover, n_span, n_trg1,n_trg2)
                        # Calculate the new score using TM, LM, RM, phrase, word
                        n_score = (l_score +
                                   log_s_given_t * lambda_tm +
                                   calc_lm_log_prob(lm_words) * lambda_lm +
                                   -abs(l_span[1]-j1) * lambda_dist +
                                   lambda_phrase +
                                   lambda_word * len(n_trg_words))
                        # If the score is better than the existing score, update the state
                        if (n_stackid not in stacks[n_finished]) or (n_score > stacks[n_finished][n_stackid][0]):
                            stacks[n_finished][n_stackid] = (n_score, l_finished, l_stackid[0], n_trg_words)
    # Find the best final hypothesis after considering the sentence ending symbol
    # in the language model
    best_score = -1e10
    best_hyp = None
    last_span = None
    for key, hyp in stacks[J].items():
        (l_score, l_prev_fin, l_prev, l_trg_words) = hyp
        n_score = l_score + calc_lm_log_prob([l_trg_words[-1], "</s>"]) * lambda_lm
        if n_score > best_score:
            best_score = n_score
            best_hyp = hyp
            last_span = key[1]

    # Get the target words in reverse order
    output = []
    while best_hyp != None:
        (l_score, l_prev_fin, l_prev, l_trg_words) = best_hyp
        if l_prev_fin == None:
            break
        # Uncomment this line if you want to see what words were translated into what
        # print "%s ||| %s" % (" ".join(src[last_span[0]:last_span[1]]), " ".join(l_trg_words))
        output.append(l_trg_words)
        best_hyp = stacks[l_prev_fin][l_prev]
        last_span = l_prev[1]

    # Finally, reverse them into correct order and print
    output.reverse()
    trg_words = itertools.chain.from_iterable(output)
    print " ".join(trg_words)
    sys.stdout.flush()
