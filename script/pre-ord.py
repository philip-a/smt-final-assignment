#!/usr/bin/python3
import sys

filt = set()
#filt.add('、')
#filt.add('？')
#filt.add('。')
for line in sys.stdin:
	line = line.strip().split()
	temp = []
	output = []

	mark = True
	for word in line:
		if word == 'は':
			if mark:
				mark = False
			else:
				for w in reversed(temp):
					output.append(w)
				temp = []
			output.append(word)
		elif mark:
			output.append(word)
		elif word in filt:
			for w in reversed(temp):
				output.append(w)
			temp = []
			output.append(word)
		else:
			temp.append(word)
	for word in (reversed(temp)):
		output.append(word)
	print (" ".join(output))
