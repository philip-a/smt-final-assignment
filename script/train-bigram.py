#!/usr/bin/python

# Load the system library
import sys

# Open the file to read
input_file = open(sys.argv[1], "r")

# Create two dictionaries to hold the counts and context counts
from collections import defaultdict
counts = defaultdict(lambda: 0)
context_counts = defaultdict(lambda: 0)

# Read the training file one at a time
for line in input_file:
    # Split the line into words and add the beginning/ending symbols
    line = line.strip()
    words = line.split(" ")
    words.insert(0, "<s>")
    words.append("</s>")
    # For every word, add its bigram and unigram
    for i in range(1, len(words)):
        bigram = " ".join( (words[i-1], words[i]) )
        counts[bigram] += 1
        context_counts[words[i-1]] += 1
        counts[words[i]] += 1
        context_counts[""] += 1
        
# Print out the n-grams probabilities
for ngram, count in sorted(counts.items()):
    words = ngram.split(" ")
    words.pop()
    context = " ".join(words)
    print "%s\t%.10f" % (ngram, float(counts[ngram])/context_counts[context])

input_file.close()
